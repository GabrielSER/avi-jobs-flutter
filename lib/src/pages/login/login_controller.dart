import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  void goToRegisterPage() {
    Get.offNamedUntil('/register', (route) => false);
  }

  void goToJobListPage() {
    Get.offNamedUntil('/joblist', (route) => false);
  }
}
