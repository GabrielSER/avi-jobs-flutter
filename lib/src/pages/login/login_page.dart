import 'dart:core';

import 'package:avijobs/src/pages/login/login_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  static const azul = Color.fromARGB(255, 38, 96, 164);
  static const claro = Color.fromARGB(255, 237, 247, 246);
  static const azulClaro = Color.fromARGB(255, 15, 163, 177);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _auth = FirebaseAuth.instance;

  LoginController con = Get.put(LoginController());

  String email = '';

  late String password;

  bool showSpinner = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: Stack(
        children: [
          _backgroundCover(),
          SingleChildScrollView(
              child: Column(
            children: [
              _imageCover(),
              const SizedBox(
                height: 100,
              ),
              _title(),
              _boxForm(context)
            ],
          ))
        ],
      ),
      )
    );
  }

  Widget _title() {
    return const Text(
      "AVI JOBS",
      style: TextStyle(color: LoginPage.azul, fontWeight: FontWeight.bold, fontSize: 30),
    );
  }

  Widget _boxForm(BuildContext context) {
    return Container(
        height: 400,
        width: double.infinity,
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        margin: const EdgeInsets.only(top: 20),
        decoration:
            BoxDecoration(color: LoginPage.azul, borderRadius: BorderRadius.circular(40)),
        child: Column(children: [
          _signIn(),
          _textFieldEmail(),
          _textFieldPassword(),
          _textViewJobs(),
          _buttonLogin(context),
          _textDontHaveAccount(),
        ]));
  }
_changeEmail(String value) async{
  final prefs = await SharedPreferences.getInstance();
  email = value;
  await prefs.setString('email', value);
}
_loadEmail() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  setState(() {
    email = prefs.getString('email') ?? '';
  });
}

@override
  void initState() {
    super.initState();
    _loadEmail();
  }
  Widget _textFieldEmail() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: TextField(
        controller: TextEditingController(text: email ?? ''),
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          hintText: 'Email',
          prefixIcon: Icon(Icons.email),
          fillColor: Colors.white,
        ),
        onChanged: (value) => _changeEmail(value),
      ),
    );
  }

  Widget _signIn() {
    return const Text(
      '---------- Sign In ----------',
      style: TextStyle(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
    );
  }

  Widget _textFieldPassword() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: TextField(
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: const InputDecoration(
            hintText: 'Password', prefixIcon: Icon(Icons.lock)),
        onChanged: (value) => password = value,
      ),
    );
  }

  Widget _imageCover() {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(top: 20, bottom: 20),
      alignment: Alignment.center,
      child: Image.asset(
        'assets/img/logo.png',
        width: 130,
        height: 130,
      ),
    ));
  }

  Widget _textDontHaveAccount() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () => con.goToRegisterPage(),
          child: const Text(
            'Create Account',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        )
      ],
    );
  }

  Widget _textViewJobs() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () => con.goToJobListPage(),
          child: const Text(
            'View Jobs',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        )
      ],
    );
  }


  Widget _buttonLogin(context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: ElevatedButton(
        onPressed: () async{
          setState(() {
            showSpinner = true;
          });
          try {
             final user = await _auth.signInWithEmailAndPassword(email: email, password: password);
             setState(() {
              showSpinner = false;
            });
            if (user != null){
              
              con.goToJobListPage();
            }
            } catch (e) {
              setState(() {
              showSpinner = false;
            });
              _showDialog(context);
              print(e);
            }
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: LoginPage.azulClaro,
          padding: const EdgeInsets.symmetric(vertical: 20),
          shape: const StadiumBorder(),
        ),
        child: const Text('LOG IN',
            style: TextStyle(color: Colors.white, fontSize: 18)),
      ),
    );
  }

Future _showDialog(context){
    return showDialog(context: context, builder: (context) =>  AlertDialog(
      title: const Text('Login Error'),
      content: const Text('Check your email and password'),
      actions: [
        TextButton(onPressed: () => Navigator.pop(context), child: const Text('Ok'))
      ],
    ));
  }

  Widget _backgroundCover() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: LoginPage.claro,
    );
  }
}
