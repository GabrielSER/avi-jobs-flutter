import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';

class JobListController extends GetxController {

  final ref = FirebaseDatabase.instance.ref().child("Jobs");
  final auth =  FirebaseAuth.instance; 
  void goToLoginPage() {
    Get.offNamedUntil('/', (route) => false);
  }

}
