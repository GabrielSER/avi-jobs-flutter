import 'dart:ui';
import 'package:avijobs/src/pages/jobList/jobList_controller.dart';
import 'package:avijobs/src/widgets/connection_status/warning_widget_wifi_notifier.dart';
import 'package:avijobs/src/widgets/navigation_bottom/navigation_bottom.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class JobList extends StatefulWidget {
  JobList({Key? key}) : super(key: key);
  static const blue = Color.fromARGB(255, 38, 96, 164);
  static const light = Color.fromARGB(255, 237, 247, 246);
  static const blueLight = Color.fromARGB(255, 15, 163, 177);
  static const green = Color.fromARGB(255, 27, 81, 45);
  static const black = Color.fromARGB(255, 0, 5, 5);

  @override
  State<JobList> createState() => _JobListState();
}

class _JobListState extends State<JobList> {
  TextEditingController searchController = TextEditingController();

 

  String search = "";

  bool showSpinner = false;

  JobListController con = Get.put(JobListController());

  void updateSearch(val) {
      search = val;
  }

  Widget _job(pImagen, pName, pVacants, pDescription, pSalary, pHours) {
    return Center(
      child: Container(
        padding: const EdgeInsets.fromLTRB(10,12,12,0),
        height: 230,
        child: Stack(
          children: [
            Positioned(child: Material(
              child: Container(
                height: 180,
                width: 400,
                decoration:  BoxDecoration(
                  color: JobList.blueLight,
                  borderRadius: BorderRadius.circular(0.0),
                ),
                child: Row(children: [
                  Container(
                    height: 180,
                    width: 80,
                  ),
                  Container(
                    height: 180,
                    width: 290,
                    color: JobList.green,
                    alignment: Alignment.centerLeft,
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            pName,
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          child: Text( "vacants: " '${pVacants ?? "Empty"}',
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          child: Text("Description: " '${pDescription ?? "Empty"}',
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          child: Text("Salary: " '${pSalary ?? "0"} COP',
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          child: Text("Working Hours: " '${pHours ?? "Empty"}',
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),//////
                ),
              ),
            ),
          ],
        ),
        
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("Job List"),
        actions:[
          IconButton(
              icon: const Icon(
                  Icons.logout,
                  color: JobList.blueLight,
                  size: 34.0),
              onPressed: (){
                
                con.auth.signOut();
                con.goToLoginPage();
              }
          ),
        ],
        backgroundColor: JobList.green,
      ),
      bottomNavigationBar: NavigationBottom(),
      backgroundColor: JobList.light,
      body: Column(children: [
        const WarningWidgetValueNotifier(),
        Expanded(child: Column(
        children: [
          TextFormField(
            controller: searchController,
            decoration: InputDecoration(
              hintStyle: const TextStyle(color: Colors.grey),
              hintText: "Search with title job",
              prefixIcon: const Icon(Icons.search, color: JobList.black,),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              
            ),
            onChanged: (String val) {
                search = val;
                  
            },
          ),
          Expanded(child: Center(
            child: FirebaseAnimatedList(
                  query: con.ref,
                  itemBuilder: (BuildContext context, DataSnapshot snapshot,
              Animation<double> animation, int index) {
                var value = Map<String, dynamic>.from(snapshot.value as Map);
                var name = value["jobName"];
                var vacants = value["vacants"];
                var description = value["jobDescription"];
                var salary = value["salary"];
                var hours = value["workingHours"];
                var image = value["companyImage"];
                
          
                if(searchController.text.isEmpty){
                  return _job(image, name, vacants, description, salary, hours);
                }else if(name.toLowerCase().contains(searchController.text.toString())){
                  return _job(image, name, vacants, description, salary, hours);
                }else {
                  return Container();
                }
                  },
                ),
          ),)
        ],
      ),),
      ],)
    );
  }
}


