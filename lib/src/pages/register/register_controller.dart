import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class RegisterController extends GetxController {
  ImagePicker picker = ImagePicker();
  File? imageFile;

  Future selectImage(ImageSource imageSource) async {
    XFile? image = await picker.pickImage(source: imageSource);
    if (image != null) {
      imageFile = File(image.path);
      update();
    }
  }

  bool hayImagen() {
    if (imageFile == null) {
      return false;
    }
    return true;
  }

  void goToJobListPage() {
    Get.offNamedUntil('/joblist', (route) => false);
  }

  void goToLoginPage() {
    Get.offNamedUntil('/login', (route) => false);
  }

  void goToCreateJobPage() {
    Get.offNamedUntil('/create-job', (route) => false);
  }
  Future _showMessage(context, title, description){
    return showDialog(context: context, builder: (context) =>  AlertDialog(
      title: Text(title),
      content: Text(description),
      actions: [
        TextButton(onPressed: () => Navigator.pop(context), child: Text('Ok'))
      ],
    ));
  }
    void showMessage(context, title, description){
      _showMessage(context, title, description);
    }

  void showAlertDialog(BuildContext context) {
    Widget galleryButton = ElevatedButton(
        onPressed: () {
          Get.back();
          selectImage(ImageSource.gallery);
        },
        child: const Text(
          'Galería',
          style: TextStyle(color: Colors.white),
        ));

    Widget cameraButton = ElevatedButton(
        onPressed: () {
          Get.back();
          selectImage(ImageSource.camera);
        },
        child: const Text(
          'CAMARA',
          style: TextStyle(color: Colors.white),
        ));

    AlertDialog alertDialog = AlertDialog(
      title: const Text('Selecciona una opción'),
      actions: [galleryButton, cameraButton],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
    
  }
}