import 'package:avijobs/src/pages/jobList/jobList.dart';
import 'package:avijobs/src/pages/register/register_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:flutter/src/widgets/navigator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  static const azul = Color.fromARGB(255, 38, 96, 164);
  static const claro = Color.fromARGB(255, 237, 247, 246);
  static const azulClaro = Color.fromARGB(255, 15, 163, 177);
  static const verde = Color.fromARGB(255, 27, 81, 45);
  static const negro = Color.fromARGB(255, 0, 5, 5);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String email = '';

  String password = '';

  String passwordRepeat = '';

  bool showSpinner = false;

  final _auth = FirebaseAuth.instance;

  RegisterController con = Get.put(RegisterController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _backgroundCover(),
          SingleChildScrollView(
              child: Column(
            children: [_imageCover(context), _title(), _boxForm(context)],
          ))
        ],
      ),
    );
  }

  _changeEmail(String value) async{
  final prefs = await SharedPreferences.getInstance();
  email = value;
  await prefs.setString('RegisterEmail', value);
}

_loadEmail() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  setState(() {
    email = prefs.getString('email') ?? '';
  });
}

@override
  void initState() {
    super.initState();
    _loadEmail();
  }

  Widget _title() {
    return const Text(
      '---------- Register ----------',
      style: TextStyle(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30),
    );
  }

  Widget _boxForm(BuildContext context) {
    return Container(
        height: 600,
        width: double.infinity,
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        margin: const EdgeInsets.only(top: 20),
        decoration:
            BoxDecoration(color: RegisterPage.azul, borderRadius: BorderRadius.circular(30)),
        child: Column(children: [
          _filaWorkerEmployer(),
          _textFieldEmail(),
          _textFieldPassword(),
          _textFieldPasswordRepeat(),
          _buttonRegister(context),
          _textDontHaveAccount()
        ]));
  }

  Widget _imageCover(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: const EdgeInsets.only(top: 25),
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: () => con.showAlertDialog(context),
        child: GetBuilder<RegisterController>(
          builder: (value) => CircleAvatar(
            backgroundImage: con.hayImagen()
                ? FileImage(con.imageFile!)
                : const AssetImage('assets/img/user.png') as ImageProvider,
            radius: 60,
          ),
        ),
      ),
    ));
  }

  Widget _textFieldEmail() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child:  TextField(
        controller: TextEditingController(text: email ?? ''),
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          hintText: 'Email',
          prefixIcon: Icon(Icons.email),
          fillColor: Colors.white,
        ),
        onChanged: (value) => _changeEmail(value),
      ),
    );
  }

  Widget _textFieldPassword() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: TextField(
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: const InputDecoration(
            hintText: 'Passsword', prefixIcon: Icon(Icons.lock)),
        onChanged: (value) => password = value,
      ),
    );
  }

  Widget _textFieldPasswordRepeat() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: TextField(
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: const InputDecoration(
            hintText: 'Repeat password', prefixIcon: Icon(Icons.lock)),
        onChanged: (value) => passwordRepeat = value,
      ),
    );
  }

  Widget _textDontHaveAccount() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () => con.goToLoginPage(),
          child: const Text(
            '<- Go back',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
        )
      ],
    );
  }

  Widget _buttonRegister(context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: ElevatedButton(
        onPressed: () async {
          bool centinel = _validatePassword(context);
          if(centinel){
              try{
                final newUser = await _auth.createUserWithEmailAndPassword(
                  email: email, password: password );
                if(newUser != null){

                  con.goToJobListPage();
                }
              }
              catch(e){
                con.showMessage(context, 'Try with another email', 'The user already exists');
                print(e);
              }
          }else{

          }
        },
        style: ElevatedButton.styleFrom(
          primary: RegisterPage.azulClaro,
          padding: const EdgeInsets.symmetric(vertical: 20),
          shape: const StadiumBorder(),
        ),
        child: const Text('REGISTER',
            style: TextStyle(color: Colors.white, fontSize: 18)),
      ),
    );
  }

  Widget _filaWorkerEmployer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _registerButton('assets/img/worker-icon-bg.png', 'Worker'),
        _registerButton('assets/img/list-icon-bg.png', 'Employer')
      ],
    );
  }

  Widget _registerButton(pImagen, pDescription) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: RegisterPage.azulClaro, borderRadius: BorderRadius.circular(25)),
          child: Image.asset(
            pImagen,
            width: 100,
            height: 100,
            color: RegisterPage.azul,
          ),
        ),
        Text(pDescription)
      ],
    );
  }

  Widget _backgroundCover() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: RegisterPage.azul,
    );
  }

  bool _validatePassword(context) {
    
    if(email == ''){
      con.showMessage(context, 'Wrong email', 'Please enter a valid email');
      return false;
    }
    else if(email.contains('@') == false){
      con.showMessage(context, 'Wrong email', 'Please enter a valid email');
      return false;
    }
    else if(email.contains('.') == false){
      con.showMessage(context, 'Wrong email', 'Please enter a valid email');
      return false;
    }
    else if(password == '' || passwordRepeat == '') {
      con.showMessage(context, 'Wrong password', 'The password can not be empty');
      return false;
    }
    else if (password != passwordRepeat) {
      con.showMessage(context, 'Wrong password', 'The passwords are not the same');
      return false;
    } else if(passwordRepeat.length<6 || password.length < 6) {
      con.showMessage(context, 'Wrong password', 'The password must be at least 6 characters');
      return false;
    } 
    return true;
  }
}