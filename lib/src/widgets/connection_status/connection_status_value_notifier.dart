import 'dart:async';
import 'package:avijobs/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:avijobs/src/utils/check_internet_connection.dart';

class ConnectionStatusValueNotifier extends ValueNotifier<ConnectionStatus> {
  late StreamSubscription _connectionSubscription;

  ConnectionStatusValueNotifier() : super(ConnectionStatus.online) {
    _connectionSubscription = internetChecker
        .internetStatus()
        .listen((newStatus) => value = newStatus);
  }

  @override
  void dispose() {
    _connectionSubscription.cancel();
    super.dispose();
  }
}