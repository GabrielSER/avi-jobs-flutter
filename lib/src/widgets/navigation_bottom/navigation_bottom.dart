import 'package:avijobs/src/pages/createJob/createJob.dart';
import 'package:avijobs/src/widgets/navigation_bottom/navigation_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NavigationBottom extends StatelessWidget{

  NavigationController con = Get.put(NavigationController());

  NavigationBottom({super.key});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          label: 'Job list',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business),
          label: 'Create Job',

        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: 'User',
        ),
      ],
      onTap: (value) => {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => con.changeIndex(value)),
      )
      },
    );
  }

}