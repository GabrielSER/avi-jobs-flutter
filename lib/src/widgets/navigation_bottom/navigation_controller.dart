import 'package:avijobs/src/pages/createJob/createJob.dart';
import 'package:avijobs/src/pages/jobList/jobList.dart';
import 'package:avijobs/src/pages/login/login_page.dart';
import 'package:avijobs/src/pages/viewProfile/viewProfile_page.dart';
import 'package:get/get.dart';

class NavigationController extends GetxController {



  changeIndex(int index) {

    switch (index) {
      case 0:
        return JobList();
      case 1:
        return CreateJob();
      case 2:
        return LoginPage();
      default:
        return JobList();
    }
  }

}