import 'package:avijobs/src/pages/createJob/createJob.dart';
import 'package:avijobs/src/pages/jobDescription/jobDescription_page.dart';
import 'package:avijobs/src/pages/jobList/jobList.dart';
import 'package:avijobs/src/pages/login/login_page.dart';
import 'package:avijobs/src/pages/viewProfile/viewProfile_page.dart';
import 'package:avijobs/src/utils/check_internet_connection.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'src/pages/register/register_page.dart';

// Usually this will be initialized one time at the start of the app
final internetChecker = CheckInternetConnection();
Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    // Replace with actual values
    options: const FirebaseOptions(
      apiKey: 'AIzaSyDQaxUS1a0DLV1U4y9z09buzmUq9gt1vEI',
    appId: '1:623719423426:android:d3de8c6f4f59618cc90a5b',
    messagingSenderId: '623719423426',
    projectId: 'isis3520-202220-team25-a0abb',
    databaseURL: 'https://isis3520-202220-team25-a0abb-default-rtdb.firebaseio.com',
    storageBucket: 'isis3520-202220-team25-a0abb.appspot.com',
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }
// comentario para que se guarde equis de
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Moviles',
      debugShowCheckedModeBanner: false,
      initialRoute: '/joblist',
      getPages: [
        GetPage(name: '/login', page: () => LoginPage()),
        GetPage(name: '/register', page: () => RegisterPage()),
        GetPage(name: '/joblist', page: () =>  JobList()),
        GetPage(name: '/create-job', page: () => const CreateJob()),
        GetPage(name: '/job-detail', page: () => const JobDescriptionPage()),
        GetPage(name: '/view-profile', page: () =>  ViewProfilePage()),
      ],
      navigatorKey: Get.key,
    );
  }
}
//hhgg